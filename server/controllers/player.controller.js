import Player from '../models/player.model';

exports.index = async(req, res) => {
    const players = await Player.find();
    res.json(players);
};

exports.create = async(req, res) => {
    if(!req.body.gamer){
        return res.status(500).send({
            message: "All fields are required !"
        });
    }
    const { gamer, kills, deaths } = req.body;
    const player = new Player({ gamer, kills, deaths });
    await player.save();
    res.json({status: "Player saved successfully !"}); 
};

exports.update = async(req, res) => {
    if(!req.body.gamer){
        return res.status(500).send({
            message: "All fields are required !"
        });
    }
    const { _id ,gamer, kills, deaths } = req.body;
    const newPlayer = { _id, gamer, kills, deaths };
    await Player.findOneAndUpdate({ newPlayer });
    res.json({ status: "Player updated successfully !"});
};

exports.delete = async(req, res) => {
    await Player.findOneAndDelete(req.params.id);
    res.json({ status: "Player removed successfully !" });
};