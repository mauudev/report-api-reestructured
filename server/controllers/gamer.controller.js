import Gamer from '../models/gamer.model';

exports.index = async(req, res) => {
    const gamers = await Gamer.find();
    res.json(gamers);
};
// 
exports.create = async(req, res) => {
    if(!req.body.nickname){
        return res.status(500).send({
            message: "Invalid input !"
        });
    }
    const { nickname } = req.body;
    const gamer = new Gamer({ nickname });
    await gamer.save();
    res.json({status: 'Gamer saved !'});
};

exports.update = async(req, res) => {
    if(!req.body.nickname){
        return res.status(500).send({
            message: "Invalid input !"
        });
    }
    const { nickname } = req.body;
    const newGamer = { nickname };
    await Gamer.findByIdAndUpdate(req.params.id, newGamer);
    res.json({status: 'Gamer updated !'});
};

exports.delete = async(req, res) => {
    await Gamer.findByIdAndRemove(req.params.id);
    res.json({status: 'Gamer removed !'});
};