import Room from '../models/room.model';

exports.index = async(req, res) => {
    const rooms = await Room.find();
    res.json(rooms);
};

exports.create = async(req, res) => {
    if(!req.body.name){
        return res.status(500).send({
            message: "All fleids are required !"
        });
    }
    const { name, team_one, team_two, isFull } = req.body;
    const room = new Room({ name, team_one, team_two, isFull });
    await room.save();
    res.json({status: "Room saved !"});
};