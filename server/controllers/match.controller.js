import Match from '../models/match.model';
import Player from '../models/player.model';

exports.index = async(req, res) => {
    const matches = await Match.find();
    res.json(matches);
};

exports.create = async(req, res) => {
    if(!req.body.team_one || !req.body.team_two){
        return res.status(500).send({
            status: "All fields is required !"
        });
    }
    const { room, team_one, team_two } = req.body;
    
    var players_team_one = team_one.map(gamer_id => {
        var player = new Player({ gamer: gamer_id, kills: 0, deaths: 0, isAlive: true});
        player.save().then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'An error occurred while create a player !'
            });
        });
        return player;
    });

    var players_team_two = team_two.map(gamer_id => {
        var player = new Player({ gamer: gamer_id, kills: 0, deaths: 0, isAlive: true});
        player.save().then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'An error occurred while create a player !'
            });
        });
        return player;
    });

    var match = new Match({
        room: room, 
        team_one: players_team_one, 
        team_two: players_team_two,
        finished: false
    });

    await match.save();
    res.json({status: "Match saved successfully !"});
};

exports.delete =  async(req, res) => {
    await Match.findOneAndDelete(req.params.id);
    res.json({status: "Match removed successfully !"});
};