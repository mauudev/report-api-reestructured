import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const MatchScoreSchema = new Schema({
    match: {
        type: ObjectId,
        ref: 'Match'
    },
    winner: {
        type: String,
        required: true
    }
});

export default mongoose.model('MatchScore', MatchScoreSchema);