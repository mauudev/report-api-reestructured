import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const MatchSchema = new Schema({
    room: {
        type: ObjectId,
        ref: 'Room'
    },
    team_one: [{player:{type: ObjectId, ref: 'Player'}}],
    team_two: [{player:{type: ObjectId, ref: 'Player'}}],
    finished: {
        type: Boolean,
        default: false
    }
});

MatchSchema.set({
    timestamps: { 
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default mongoose.model('Match', MatchSchema);