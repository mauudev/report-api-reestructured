import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const GamerScoreSchema = new Schema({
    gamer: {
        type: ObjectId,
        ref: 'Gamer',
    },
    kills: {
        type: Number,
        required: true
    },
    deaths: {
        type: Number,
        required: true
    }
});

export default mongoose.model('GamerScore', GamerScoreSchema);