import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const GamerSchema = new Schema({
    nickname: {
        type: String,
        required: true
    }    
});

GamerSchema.set({
    timestamps: { 
        updatedAt: 'updated_at'
    }
});

export default mongoose.model('Gamer', GamerSchema);