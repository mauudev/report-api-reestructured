import mongoose, { model } from 'mongoose';

const Schema = mongoose.Schema;

const RoomSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    team_one: [{ type: Schema.ObjectId, ref: "Gamer"}],
    team_two: [{ type: Schema.ObjectId, ref: "Gamer"}],
    isFull: {
        type: Boolean,
        default: false
    }
});

RoomSchema.set({
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at"
    }
});

export default mongoose.model("Room", RoomSchema);