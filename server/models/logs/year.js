import mongoose from 'mongoose';
import gamer from '../gamer';

const Schema = mongoose.Schema;

const YearSchema = new Schema({    
    player: {
        type: String,
        required: true,
    },
    score: {
        type: Number,
        required: true
    },   
    kill: { 
        type: Number,
        required: true
    },
    death: { 
        type: Number,
        required: true
    }
});

YearSchema.set({
    timestamps: { 
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

export default mongoose.model('YearLogger', YearSchema);