import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const PlayerSchema = new Schema({
    gamer: {
        type: ObjectId,
        ref: 'Gamer'
    },
    kills: {
        type: Number,
        required: true
    },
    deaths: {
        type: Number,
        required: true
    },
    isAlive: {
        type: Boolean,
        default: true
    }   
});

PlayerSchema.set({
    timestamps: { 
        updatedAt: 'updated_at'
    }
});

export default mongoose.model('Player', PlayerSchema);