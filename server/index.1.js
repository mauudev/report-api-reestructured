import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import path from 'path';

const app = express();
const server = http.createServer(app)
const io = socketIO(server);

const host = "127.0.0.1";
const port = "4000";

// Configuring the database
const dbConfig = require('./config/database.js');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// View renderizing
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');

//static files
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
// const io = require('socket.io')();

app.use('/', require('./routes/server.routes'));   
require('./routes/socket.routes.js')(app,io);

server.listen(port, host, () => {
    // console.log(`APP: ${get('app')}`)
    console.log(`Server Created, Ready to listen at ${host}:${port}`)
});
