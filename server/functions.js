 'use strict';

 module.exports = {
   sendData: sendData
 };

 const DATA = [
    {killer_id: "player_1", dead_id:"player_3"},
    {killer_id: "player_1", dead_id:"player_4"},
    {killer_id: "player_2", dead_id:"player_1"},
    {killer_id: "player_5", dead_id:"player_3"},
    {killer_id: "player_7", dead_id:"player_3"},
    {killer_id: "player_8", dead_id:"player_1"},
    {killer_id: "player_1", dead_id:"player_2"},
    {killer_id: "player_2", dead_id:"player_10"},
    {killer_id: "player_5", dead_id:"player_6"},
 ];
 
 function sendData(context, events, done) {
   const index = Math.floor(Math.random() * DATA.length);
   context.vars.datos = DATA[index];
   return done();
 }