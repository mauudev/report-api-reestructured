import mongoose from 'mongoose';
import config from '../config';

mongoose.Promise = global.Promise;


before((done) => {
  mongoose.connect(config.mongodb_test);
  mongoose.connection
  .once('open', () => { 
      done(); 
  })
  .on('error', (error) => {
    console.warn('Error', error);
  });
});

/*beforeEach( (done) => {
  const { gamers, scores, matchs } = mongoose.connection.collections;
  gamers.drop(() => {
    scores.drop(() => {
      matchs.drop(() => {
        done();
      });
    });
  });
});*/
