import assert from 'assert';
import Gamer from '../../models/gamer';

describe('Creating users', () => {
  it('saves a gamer', (done) => {
    let someGamer = new Gamer({ 
        nick: 'someUser1'
    });

    someGamer.save()
      .then(() => {
        assert(!someGamer.isNew);
        done();
      });
  });
});