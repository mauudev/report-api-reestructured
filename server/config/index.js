import dotenv from 'dotenv';
import path from 'path';

const config = dotenv.config({
  path: path.resolve(process.cwd(), '.env')
});

export default Object.assign({}, {
    port : config.parsed.PORT,
    mongodb: config.parsed.MONGODB_URL,
    mongodb_test: config.parsed.MONGODB_URL_TEST
});
