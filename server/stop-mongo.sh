#!/bin/bash

echo "Stopping mongodb"

docker stop mongodb

echo "Removing container"

docker rm mongodb

echo  "MongoDB was stopped"
