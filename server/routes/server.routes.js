import express from 'express';
import GamerController from '../controllers/gamer.controller';
import PlayerController from '../controllers/player.controller';
import RoomController from '../controllers/room.controller';
import MatchController from '../controllers/match.controller';

const router = express.Router();

router.get('/gamers', GamerController.index);
router.post('/gamers/create', GamerController.create);
router.put('/gamers/:id/update', GamerController.update);
router.delete('/gamers/:id/delete', GamerController.delete);
// router.get('/gamers/match-report', GamerController.report);

router.get('/players', PlayerController.index);
router.post('/players/create', PlayerController.create);
router.put('/gamers/:id/update', PlayerController.update);
router.delete('/gamers/:id/delete', PlayerController.delete);

router.get('/rooms', RoomController.index);
router.post('/rooms/create', RoomController.create);

router.get('/matches', MatchController.index);
router.post('/matches/create', MatchController.create);

module.exports = router;