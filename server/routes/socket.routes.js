
module.exports = (app, io) => {
    const reportController = require('../controllers/report.controller');

    app.get('/match-report', async(req, res, next) => {
        
        next();
    },  reportController.matchReport);
}