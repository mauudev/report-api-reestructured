#!/bin/bash

echo "Initializing mongodb"

docker run --name mongodb -p 27017:27017 -d mongo

echo "MongoDB is running"
