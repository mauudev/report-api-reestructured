#include <iostream>
#include <cstring>

using namespace std;

struct Node
{
    size_t len;
    char* value;
    Node *next;
};

struct StringLinkedList
{
    Node *first;
    Node *last;
    size_t size;
};

void init(StringLinkedList *list)
{
    list->first = list->last = nullptr;
    list->size = 0;
}

void add_tail(StringLinkedList *list, const char *value)
{
    auto len = strlen(value);
    auto nn = new char[len+1];
    memcpy(nn,value,len+1);
    auto node = new Node{len, nn, nullptr};
    if(list->size == 0)
    {
        list->first = list->last = node;
        list->size ++;
        return ;
    }
    list->last->next = node;
    list->last = node;
    list->size ++;
}

void add_head(StringLinkedList *list, const char* value)
{
    auto len = strlen(value);
    auto nn = new char[len+1];
    memcpy(nn,value,len+1);
    auto node = new Node{len, nn, nullptr};
    if(list->size == 0)
    {
        list->first = list->last = node;
        list->size ++;
        return ;
    } 
    node->next = list->first;
    list->first = node;
    list->size ++;
}

void print(StringLinkedList *list)
{
    Node *aux = list->first;
    while(aux != nullptr)
    {
        cout << aux->value << "\n";
        aux = aux->next;
    }
    cout << "Size: " << list->size << "\n";
}

void release(StringLinkedList *list)
{
    auto aux = list->first;
    size_t len = list->size;
    while(aux != nullptr)
    {
        auto next = aux->next;
        delete aux;
        aux = next;
        cout << "Eliminado " << len -- << "\n";
    }
    list->size = 0;

}

int main()
{
    StringLinkedList list;
    init(&list);
    add_tail(&list, "Hello");
    add_tail(&list, "world");
    add_tail(&list, "today");
    add_head(&list, "is");
    add_head(&list, "Wednesday");
    add_head(&list, "yeah");
    print(&list);
    release(&list);
}