const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let PlayerSchema = new Schema({
    nickname : {type: String, required: true, max: 100},   
}, {
    timestamps: true
});


//Export model
module.exports = mongoose.model('Player',PlayerSchema);