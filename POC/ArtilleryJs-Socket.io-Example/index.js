// app.js
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const Rank = require('./models/rank.model');
const cron = require("node-cron");

// initialize our express app
const app = express();

// Configuring the database
const dbConfig = require('./config/database.config.js');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// Set body parser for parse incoming requests bodies in a middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// View renderizing
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');

//static files
app.use(express.static(path.join(__dirname, 'public')));

// Imports routes for the app
app.get('/', (req, res) => {
    res.json({"message": "Welcome to killem application. Take killem quickly. Organize and keep track of all your killem."});
});

// Up server
let port = 3000;

const server = app.listen(port, () => {    
    console.log('Server is up and running on port numner ' + port);
});

// import socket.io
const socketIO = require('socket.io');

// socketIO.listen(server);//aca le pasamos el server creado
const io = socketIO(server);//aca le pasamos el server creado y podemos guardar en una const que es la comunicacion con websockets
// -> Hasta aqui ya esta configurado el socket.io ahora podemos iniciar las comunicaciones
var killer = {firstName:"John", lastName:"Doe", age:46};


//esto es el socket de raichu team
// io.on('connection', async(socket) => { //aca enviamos el socket del cliente al server
//     console.log('New connection to Rainchu Team !', socket.id);
//     // var index = Math.floor(Math.random() * (data.length)); 
//     // var data_players = data[index];
//     socket.on('raichu:data', (data) => {
//         // we tell the client to execute 'new message'
//         socket.broadcast.emit('new message', {
//           username: socket.username,
//           message: data
//         });
//     io.sockets.emit('raichu:data', socket.datos);// reenviamos los datos a todos los conectados
// });

// Require killem routes
require('./routes/player.route.js')(app);
require('./routes/rank.route.js')(app);
require('./routes/score.route.js')(app);
require('./routes/main.route.js')(app,io);
require('./routes/server.route.js')(app,io);
// app.use('/', require('./routes/server.route.js'))(io);