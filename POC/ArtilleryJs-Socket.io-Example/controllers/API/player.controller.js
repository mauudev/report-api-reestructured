const Player = require('../../models/player.model');
const Score = require('../../models/score.model');
const Rank = require('../../models/rank.model');

exports.create = (req,res) => {
    if(!req.body.nickname){
        return res.status(400).send({
            message: 'Player fields can not be empty !'
        });
    }
    
    const player = new Player({
        nickname: req.body.nickname
    });

    player.save().then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || 'An error occurred while create a player !'
        });
    });
};

exports.create = async(req, res) => {
    if(!req.body.nickname){
        return res.status(500).send({
            message: 'Player fields can not be empty !'
        });
    }
    const {nickname} = req.body;
    const player = new Player({nickname});
    await player.save();
    res.send(player);
    let kills = 0;
    let deaths = 0;
    const score = new Score({kills, deaths, player});
    await score.save();
    let rankVal = 0;
    const rank = new Rank({rankVal,player});
    await rank.save();
};

exports.findAll = (req, res) => {
    Player.find().then(players => {
        res.send(players);
    }).catch(err => {
        res.status(500).send({
            message: err.message || 'An error occurred while retrieving data !'
        });
    });
};

exports.getAll = async () => {
    var players = await Player.find();
    return players
};

exports.findOne = (req, res) => {
    Player.findById(req.params.playerId).then(player => {
        if(!player){
            return res.status(400).send({
                message: 'Player with id: '+req.params.playerId + ' not found !'
            });
        }
        res.send(player);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Player not found with id " + req.params.playerId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving player with id " + req.params.playerId
        });
    });
};

exports.update = (req, res) => {
    if(!req.body.nickname){
        res.status(400).send({
            message: 'Error! fields can not be empty.'
        });
    }

    Player.findByIdAndUpdate(req.params.playerId, {
        nickname: req.body.nickname
    }, {new: true}).then(player => {
        if(!player){
            res.status(400).send({
                message: 'Error player with id: '+req.params.playerId+' not found !'
            });
        }
        res.send(player);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Player not found with id " + req.params.playerId
            });                
        }
        return res.status(500).send({
            message: "Error updating player with id " + req.params.playerId
        });
    });
};

exports.delete = (req, res) => {
    Player.findByIdAndRemove(req.params.playerId)
    .then(player => {
        if(!player){
            res.status(400).send({
                message: 'Player with id: '+req.params.playerId+' not found !'
            });
        }
        res.send({
            message: 'Player deleted successfully !'
        });
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound'){
            return res.status(404).sned({
                message: 'Player with id: '+req.params.playerId+' not found !'  
            });
        }
        return res.status(500).send({
            message: 'Player with id: '+req.params.playerId+' can not be deleted !'
        });
    });
};