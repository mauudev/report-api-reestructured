const Rank = require('../../models/rank.model');

exports.create = (req, res) => {
    if(!req.body.rank){
        return res.status(400).send({
            message: 'Rank fields can not be empty !'
        });
    }
    const rank = new Rank({
        rank: req.body.rank,
        player: req.body.player
    });

    rank.save().then(data => {
        res.send(rank);
    }).catch(err => {
        res.status(500).send({
            message: err.message || 'An error occurred while inserting data !'
        });
    });
};

exports.findAll = (req, res) => {
    Rank.find().then(ranks => {
        res.send(ranks);
    }).catch(err => {
        res.status(500).send({
            message: err.message || 'An error occurred while retrieving data !'
        });
    });
};

exports.findOne = (req, res) => {
    Rank.findById(req.params.rankId).then(rank => {
        if(!rank){
            return res.status(400).send({
                message: 'Rank with id: '+req.params.rankId+' not found !'
            });
        }
        res.send(rank);
    }).catch(err => {
        if(err.kind === 'ObjectId'){
            return status(404).send({
                message: 'Rank with id: '+req.params.rankId+' not found !'
            });
        }
        return res.status(500).send({
            message: 'Rank with id: '+req.params.rankId+' not found !'
        });
    });
};

exports.update = (req, res) => {
    if(!req.body.rank){
        res.status(500).send({
            message: 'Error ! fields can not be empty.'
        });
    }
    Rank.findByIdAndUpdate(req.params.rankId, {
        rank: req.body.rank,
        player: req.body.player
    },{new: true}).then(rank => {
        if(!rank){
            res.status(400).send({
                message: 'Rank with id: '+req.params.rankId+' not found !'
            });
        }
        res.send(rank);
    }).catch(err => {
        if(err.kind === 'ObjectId'){
            return res.status(404).send({
                message: 'Rank with id: '+req.params.rankId+' not found !'
            });
        }
        return status(500).send({
            message: 'Rank with id: '+req.params.rankId+' not found !'
        });
    });
};

exports.delete = (req, res) => {
    Rank.findByIdAndRemove(req.params.rankId)
    .then(rank => {
        if(!rank){
            res.status(400).send({
                message: 'Rank with id: '+req.params.rankId+' not found !'
            });
        }
        res.send({
            message: 'Rank deleted successfully !'
        });
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound'){
            return res.status(404).send({
                message: 'Rank with id: '+req.params.rankId+' not found !'
            });
        }
        return res.status(500).send({
            message: 'Rank with id: '+req.params.rankId+' not found !'
        });
    });
};