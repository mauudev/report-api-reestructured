const Rank = require('../../models/rank.model');
const Player = require('../../models/player.model');
const Score = require('../../models/score.model');
const cron = require("node-cron");

//schedule tasks to be run on the server
// cron.schedule("* * * * * *", async(req,res) => {
//     console.log("---------------------");
//     const someScore_1 = await Score.aggregate([{$sample: {size: 1}}]);
//     const someScore_2 = await Score.aggregate([{$sample: {size: 1}}]);
//     while(someScore_1[0]._id === someScore_2[0]._id){
//         someScore_2 = await Score.aggregate([{$sample: {size: 1}}]);
//     }

//     const queryKiller = {_id: someScore_1[0]._id};
//     const queryDead = {_id: someScore_2[0]._id};
    
//     try {
//         await Score.findOneAndUpdate(queryKiller,
//             { $inc:{ kills: 1 }},{new :true}); 
//     } catch (error) {
//         console.log('Error updating !');    
//     }
    
//     try {
//         const target = Score.findOne({_id: queryDead._id});
//         if(target.deaths == 0){
//             await Score.findOneAndUpdate(queryDead,
//                 { deaths: 0},{new :true});
//         }else{
//             await Score.findOneAndUpdate(queryDead,
//                 { $inc:{deaths: 1} },{new :true});
//         }
//     } catch (error) {
//         console.log('Error updating !');
//     }
// });

exports.index = async(io,res) => {
    const players = await Player.find();
    const ranks = await Rank.find();
    const scores = await Score.find().sort({kills: -1}).populate('player');

    res.render('index',{players:players, ranks:ranks, scores:scores});
};
