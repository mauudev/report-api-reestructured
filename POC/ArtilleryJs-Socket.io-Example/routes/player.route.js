module.exports = (app) => {
    const player = require('../controllers/API/player.controller.js');

    // Create a new player
    app.post('/player/create', player.create);

    // Retrieve all player
    app.get('/player', player.findAll);

    // Retrieve a single player with playerId
    app.get('/player/:playerId/find', player.findOne);

    // Update a player with playerId
    app.put('/player/:playerId/update', player.update);

    // Delete a player with playerId
    app.delete('/player/:playerId/delete', player.delete);
}