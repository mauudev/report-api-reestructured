const Rank = require('../models/rank.model');
const Player = require('../models/player.model');
const Score = require('../models/score.model');
const cron = require("node-cron");

module.exports = (app,io) => {
    const main = require('../controllers/API/main.controller.js');
    // io.on('connection', async(socket) => { //aca enviamos el socket del cliente al server
    //     console.log('New connection !', socket.id);
    //     cron.schedule("* * * * * *", async(req,res) => {
    //         const players = await Player.find();
    //         const ranks = await Rank.find();
    //         const scores = await Score.find().sort({kills: -1}).populate('player');
    //         io.sockets.emit('data', {players:players, ranks:ranks, scores:scores});// reenviamos los datos a todos los conectados
    //     });
    // });
    app.get('/report', main.index);
}

