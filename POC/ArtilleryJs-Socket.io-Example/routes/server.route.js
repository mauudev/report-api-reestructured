const Player = require('../models/player.model');
const Score = require('../models/score.model');

module.exports = (app, io) => {
    const game = require('../controllers/GameContext/game.controller.js');

    app.get('/match-report', async(req, res, next) => {
        io.on('connection', async(socket) => { //aca enviamos el socket del cliente al server
            console.log('New connection !', socket.id);
            socket.on('raichu:data', async(data) => {//aca escuchamos y renderizamos los mensajes enviados por el servidor
                // var killer = await Player.findOne({ _id: data.killer_id });
                // var dead = await Player.findOne({ _id: data.dead_id });
        
                // var res = {killer_nickname : killer.nickname, dead_nickname : dead.nickname}
                var res = {killer_nickname : data.killer_id, dead_nickname : data.dead_id}
                io.sockets.emit('raichu:data', res);// reenviamos los datos a todos los conectados
            });
        });
        next();
    },  game.playGame);
}