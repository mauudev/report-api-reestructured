
    const socket = io.connect('http://localhost:3000/');//esta variable se encarga los eventos del cliente al servidor
    // DOM elements
    
    $("#removeBtn").on('click', function(){
        rowData.html('')
    });
  
    let cont = 0;
    socket.on('data', function(data){//aca escuchamos y renderizamos los mensajes enviados por el servidor
        //console.log(data.scores.length)
        $(".rowData").remove()
        var template = ''
        for(var i = 0; i < data.scores.length; i++){
            template += '<tr class="rowData">'
            template += '<td>'+data.scores[i]._id+'</td>'
            template += '<td>'+data.scores[i].player.nickname+'</td>'
            template += '<td>'+data.scores[i].kills+'</td>'
            template += '<td>'+data.scores[i].deaths+'</td>'
            template += "</tr>"
        }
        console.log(template)
        $(".dataTable").append(template);
    });