module.exports = (app) => {
    const score = require('../controllers/score.controller.js');

    // Create a new score
    app.post('/score/create', score.create);

    // Retrieve all score
    app.get('/score', score.findAll);

    // Retrieve a single score with scoreId
    app.get('/score/:scoreId/find', score.findOne);

    // Update a score with scoreId
    app.put('/score/:scoreId/update', score.update);

    // Delete a score with scoreId
    app.delete('/score/:scoreId/delete', score.delete);
}